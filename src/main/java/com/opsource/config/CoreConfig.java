package com.opsource.config;

import com.opsource.server.parser.ServerParser;
import com.opsource.server.parser.XmlServerParserImpl;
import com.opsource.server.reader.FileServerReaderImpl;
import com.opsource.server.reader.ServerReader;
import com.opsource.service.ServerService;
import com.opsource.service.impl.ServerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CoreConfig 
{
    @Autowired
    Environment env;
    
    @Bean
    public ServerService serverService() {
        return new ServerServiceImpl();
    }
    
    @Bean
    public ServerReader serverReader() {
        String path = String.format("%s/%s", System.getProperty("user.dir"), env.getProperty("server.path"));
        
        FileServerReaderImpl fileServerReaderImpl = new FileServerReaderImpl();
        fileServerReaderImpl.setServerFolder(path);
        return fileServerReaderImpl;
    }
    
    @Bean
    public ServerParser serverParser() {
        XmlServerParserImpl xmlServerParserImpl = new XmlServerParserImpl();
        xmlServerParserImpl.setSchemaFile("classpath:server.xsd");
        return xmlServerParserImpl;
    }
}