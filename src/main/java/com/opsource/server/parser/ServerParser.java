package com.opsource.server.parser;

import com.opsource.model.Server;
import java.io.InputStream;

/**
 *
 * @author ALAN
 */
public interface ServerParser {

    public Server parseServer(InputStream inputStream);
}
