package com.opsource.server.parser;

import com.opsource.model.Server;
import com.opsource.server.ServerReaderException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author ALAN
 */
public class XmlServerParserImpl implements ServerParser {

    private String schemaFile;
    
    public void setSchemaFile(String schemaFile) {
        this.schemaFile = schemaFile;
    }
    
    public String getSchemaFile() {
        return schemaFile;
    }
    
    @Override
    public Server parseServer(InputStream inputStream) throws ServerReaderException {
        
        try {
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            domFactory.setNamespaceAware(true);
            
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            Document document = builder.parse(inputStream);
    
            validate(document);
                    
            return getServerFromXml(document);
        }
        catch(ParserConfigurationException exception) {
            throw new ServerReaderException("Failed to build parser ", exception);
        }
        catch(IOException exception) {
            throw new ServerReaderException("Failed to read stream ", exception);
        }
        catch(SAXException exception) {
            throw new ServerReaderException("Failed to read xml ", exception);
        }
        catch(XPathExpressionException exception) {
            throw new ServerReaderException("Failed to parse xml ", exception);
        }
    }

    private boolean validate(Document document) {
        
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // load a WXS schema, represented by a Schema instance
            DefaultResourceLoader loader = new DefaultResourceLoader();
            Resource resource = loader.getResource(getSchemaFile()); 
            
            Source schemaFile = new StreamSource(resource.getInputStream());
            Schema schema = factory.newSchema(schemaFile);

            // create a Validator instance, which can be used to validate an instance document
            Validator validator = schema.newValidator();

            // validate the DOM tree
        
            validator.validate(new DOMSource(document));
        } catch (SAXException exception) {
            throw new ServerReaderException("Invalid server xml ", exception);
        } catch(IOException exception) {
            throw new ServerReaderException("Invalid server xsd ", exception);
        }
        
        return true;
    }
    
    private Server getServerFromXml(Document document) throws XPathExpressionException {
        
        XPath xpath = XPathFactory.newInstance().newXPath();  

        Server server = new Server();
        server.setId(getServerId(document, xpath));
        server.setName(getServerName(document, xpath));
        return server;
    }
    
    private Long getServerId(Document document, XPath xpath) throws XPathExpressionException {
        
        XPathExpression expr = xpath.compile("/server/id/text()");
        String result = expr.evaluate(document, XPathConstants.STRING).toString();
        return Long.parseLong(result);
    }
    
    private String getServerName(Document document, XPath xpath) throws XPathExpressionException {
        
        XPathExpression expr = xpath.compile("/server/name/text()");
        return expr.evaluate(document, XPathConstants.STRING).toString();
    }
}
