package com.opsource.server;

/**
 *
 * @author ALAN
 */
public class ServerReaderException extends RuntimeException {
    
    public ServerReaderException(String message, Exception exception) {
        super(message, exception);
    }
}
