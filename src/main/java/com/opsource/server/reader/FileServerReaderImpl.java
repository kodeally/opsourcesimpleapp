package com.opsource.server.reader;

import com.opsource.model.Server;
import com.opsource.server.ServerReaderException;
import com.opsource.server.parser.ServerParser;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ALAN
 */
public class FileServerReaderImpl implements ServerReader {

    @Autowired
    private ServerParser serverParser;
    
    private String serverFolder;
    
    /**
     * @return the serverFolder
     */
    public String getServerFolder() {
        return serverFolder;
    }

    /**
     * @param serverFolder the serverFolder to set
     */
    public void setServerFolder(String serverFolder) {
        this.serverFolder = serverFolder;
    }
    
    private String getFullFilePath() {
        return String.format("%s%s", getServerFolder(), "server_1.xml");
    }
    
    @Override
    public Server readServer() throws ServerReaderException {

        InputStream fileInputStream = null;
        
        try {
            fileInputStream = new FileInputStream(getFullFilePath());
            return serverParser.parseServer(fileInputStream);
        }
        catch (FileNotFoundException exception) {
            throw new ServerReaderException(String.format("Failed to find file %s", getFullFilePath()), exception);
        }
        finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                }
                catch(Exception ex) {}
            }
        }
    }
}
