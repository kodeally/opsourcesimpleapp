package com.opsource.server.reader;

import com.opsource.model.Server;

/**
 *
 * @author ALAN
 */
public interface ServerReader {

    public Server readServer();
}
