package com.opsource.dao;

import com.opsource.model.Server;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ALAN
 */
public interface ServerDao extends CrudRepository< Server, Long >{
 
   public Server findByName( String name );
   public List<Server> findAll();
}
