package com.opsource;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ALAN
 */
public class CommandLineParser {
    
    public static enum COMMAND_OPTION {HELP,QUIT,ADDSERVER,DELETESERVER,EDITSERVER,COUNTSERVERS,LISTSERVERS};
    
    public class Command {
    
        private COMMAND_OPTION type;
        private List<String> parameters;

        /**
         * @return the type
         */
        public COMMAND_OPTION getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(COMMAND_OPTION type) {
            this.type = type;
        }

        /**
         * @return the parameters
         */
        public List<String> getParameters() {
            return parameters;
        }

        /**
         * @param parameters the parameters to set
         */
        public void setParameters(List<String> parameters) {
            this.parameters = parameters;
        }
    }
    
    public Command parse(String input) {
        
        try {
            String[] tokens = input.split(" ");
            String option = tokens[0];

            Command command = new Command();
            command.setType(COMMAND_OPTION.valueOf(option.toUpperCase()));

            List<String> parameters = new ArrayList<>(tokens.length - 1);
            for (int index = 1;index < tokens.length; index++) {
                parameters.add(tokens[index]);
            }
            command.setParameters(parameters);
            
            validateCommand(command);
            
            return command;
        }
        catch(IllegalArgumentException exception) {
            throw new RuntimeException("Invalid argument please see help.");
        }
    }
    
    private void validateCommand(Command command) {
        switch(command.type) {
            case HELP:
            case QUIT:
            case ADDSERVER:
            case COUNTSERVERS:
            case LISTSERVERS:
                if (command.getParameters().size() > 0) {
                    throw new RuntimeException("Invalid parameters specified");
                }
                break;
            case DELETESERVER:
                if (command.getParameters().size() != 1) {
                    throw new RuntimeException("Invalid parameters specified");
                }
                break;
            case EDITSERVER:
                if (command.getParameters().size() != 2) {
                    throw new RuntimeException("Invalid parameters specified");
                }
                break;
        }
    }
}
