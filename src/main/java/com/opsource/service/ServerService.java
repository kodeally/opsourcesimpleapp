package com.opsource.service;

import com.opsource.model.Server;
import java.util.List;

/**
 *
 * @author ALAN
 */
public interface ServerService {
    
    
    public Server addServer();
    public void createServer(Server server);
    public void editServer(Server server);
    public void deleteServer(Long id);
    public long getServerCount();
    public List<Server> findAll();
    public Server findById(Long id);
    public Server findByName(String name);
}
