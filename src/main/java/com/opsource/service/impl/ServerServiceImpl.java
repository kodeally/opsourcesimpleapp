package com.opsource.service.impl;

import com.opsource.dao.ServerDao;
import com.opsource.model.Server;
import com.opsource.server.reader.ServerReader;
import com.opsource.service.ServerService;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ALAN
 */
public class ServerServiceImpl implements ServerService {

    @Autowired
    private ServerReader serverReader;
    @Autowired
    private ServerDao serverDAO;
    
    @Override
    public Server addServer() {
        Server server = serverReader.readServer();
        createServer(server);
        return server;
    }
    
    @Override
    public void createServer(Server server){
        if (serverDAO.exists(server.getId())) {
            throw new RuntimeException(String.format("Server with id %d already exists", server.getId()));
        }
        serverDAO.save(server);
    }
    
    @Override
    public void editServer(Server server){
        Server target = serverDAO.findOne(server.getId());
        BeanUtils.copyProperties(server, target, new String[]{"id"});
        serverDAO.save(target);
    }
    
    @Override
    public void deleteServer(Long id) {
        serverDAO.delete(id);
    }
    
    @Override
    public long getServerCount() {
        return serverDAO.count();
    }
    
    @Override
    public List<Server> findAll() {
        return serverDAO.findAll();
    }
    
    @Override
    public Server findById(Long id) {
        return serverDAO.findOne(id);
    }
    
    @Override
    public Server findByName(String name) {
        return serverDAO.findByName(name);
    }
}
