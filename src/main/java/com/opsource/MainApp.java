package com.opsource;

import com.opsource.CommandLineParser.Command;
import com.opsource.app.config.AppConfig;
import com.opsource.model.Server;
import com.opsource.service.ServerService;
import java.io.*;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author ALAN
 */
public class MainApp
{
    private final static String NewLine = System.getProperty("line.separator");
    
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException
    {
        ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
        ServerService serverService = (ServerService)appContext.getBean("serverService");
     
        boolean running = true;

	showHelp();

	while(running)
	{
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
                CommandLineParser commandLineParser = new CommandLineParser();
                
                Command command = commandLineParser.parse(bufferedReader.readLine());
                
                switch(command.getType()) {
                    case HELP:
                        showHelp();
                        break;
                    case QUIT:
                        running = false;
                        break;
                    case COUNTSERVERS:
                        long count = serverService.getServerCount();
                        System.out.println(String.format("Server count:%d",count));
                        break;
                    case ADDSERVER: {
                            Server server = serverService.addServer();
                            System.out.println(String.format("Server %d %s has been added",server.getId(), server.getName()));
                        }
                        break;
                    case DELETESERVER: {
                            String serverId = command.getParameters().get(0);
                            serverService.deleteServer(Long.parseLong(serverId));
                            System.out.println(String.format("Server %s has been deleted",serverId));
                        }
                        break;
                    case EDITSERVER: {
                            String serverId = command.getParameters().get(0);
                            String name = command.getParameters().get(1);
                            serverService.editServer(new Server(Long.parseLong(serverId), name));
                            System.out.println(String.format("Server %s has been edited",serverId));
                        }
                        break;
                    case LISTSERVERS:
                        List<Server> servers = serverService.findAll();
                        if (servers != null && servers.size() > 0) {
                            Iterator<Server> list = servers.iterator();
                            while (list.hasNext()) {
                                Server server = list.next();
                                System.out.println(String.format("Server %d %s", server.getId(), server.getName()));
                            }
                        }
                        else {
                            System.out.println("No servers in list");
                        }
                        break;
                }
                
                System.out.println(NewLine);
            }
            catch(Exception exception) {
                System.out.println("A problem occured " + exception.getMessage());
                System.out.println(NewLine);
            }
 	}
    }

    private static void showHelp()
    {
	System.out.println("help to display this message");
	System.out.println("countServers to display the current number of servers present");
	System.out.println("addServer to display the current number of servers present");
	System.out.println("editServer to change the name of a server identified by id (takes 2 additional args - the id and the new name)");
	System.out.println("deleteServer to delete a server (takes one more arg - the id of the server to delete)");
	System.out.println("listServers to display details of all servers servers");
        System.out.println(NewLine);
    }
}