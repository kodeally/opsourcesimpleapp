package com.opsource.service;

import com.opsource.model.Server;
import com.opsource.test.config.TestAppConfig;
import java.util.List;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ALAN
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class,
        classes = { TestAppConfig.class })
@Transactional
@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=true)
public class ServerServiceTest {
    
    @Autowired
    ServerService serverService;
    
    @Before
    public void setUp() {
    }

    @After
    public void tearDown() throws InterruptedException {
    }

    @Test
    public void testAddServer() {
        serverService.addServer();
        List<Server> list = serverService.findAll();
        Assert.assertEquals("Add server from xml file ", 1, list.size());        
        
        Server server = list.get(0);
        Assert.assertEquals("Add server from xml file check id ", 1, server.getId().longValue());        
        Assert.assertEquals("Add server from xml file check name ", "MyServerName", server.getName());        
    }
    
    @Test
    public void testCreateServer() {
        Server server =new Server(99L,"ServerXYZ");
        serverService.createServer(server);
        Server savedServer = serverService.findById(99L);
        
        Assert.assertEquals("Create server check id ", 99, savedServer.getId().longValue());        
        Assert.assertEquals("Create server check name ", "ServerXYZ", savedServer.getName());        
    }
    
    @Test
    public void testDeleteServer() {
        Server server = new Server(1L,"Server");
        serverService.createServer(server);
        
        long count = serverService.getServerCount();
        Assert.assertEquals("Delete server check count before delete ", 1, count);        
        
        serverService.deleteServer(1L);
        
        count = serverService.getServerCount();
        Assert.assertEquals("Delete server check count after delete ", 0, count);        
    }
    
    @Test
    public void testServerCount() {
        serverService.createServer(new Server(1L,"Server1"));
        serverService.createServer(new Server(2L,"Server2"));
        serverService.createServer(new Server(3L,"Server3"));
        
        long count = serverService.getServerCount();
        Assert.assertEquals("Count server check count ", 3, count);        
    }
    
    @Test
    public void testServerList() {
        serverService.addServer();
        List<Server> list = serverService.findAll();
        
        Assert.assertEquals("List server check count ", 1, list.size());        
    }
    
    @Test
    public void testEditServer() {
        Server server = new Server(99L, "ONE");
        serverService.createServer(server);
        
        Server oldServer = serverService.findById(99L);
        Assert.assertEquals("Edit server check first name ", "ONE", oldServer.getName());        
        
        Server newServer = new Server(99L, "TWO");
        serverService.editServer(newServer);
        
        newServer = serverService.findById(99L);
        Assert.assertEquals("Edit server check first name ", "TWO", newServer.getName());        
    }
}