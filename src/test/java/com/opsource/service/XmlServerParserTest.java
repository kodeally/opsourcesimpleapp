package com.opsource.service;

import com.opsource.model.Server;
import com.opsource.server.ServerReaderException;
import com.opsource.server.parser.ServerParser;
import com.opsource.test.config.TestAppConfig;
import java.io.InputStream;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

/**
 *
 * @author ALAN
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class,
        classes = { TestAppConfig.class })
public class XmlServerParserTest {
    
    @Autowired
    ServerParser serverParser;
    
    @Before
    public void setUp() {
    }

    @After
    public void tearDown() throws InterruptedException {
    }

    @Test
    public void testReadServerXmlTest() {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("Good_Format_Server.xml");
        
        Server server = serverParser.parseServer(inputStream);
        Assert.assertEquals("Read good server xml check id ", 1, server.getId().longValue());        
    }
    
    @Test
    public void testReadServerBadXmlTest() throws ServerReaderException {
        
        try {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("Bad_Format_Server.xml");

            Server server = serverParser.parseServer(inputStream);
        }
        catch(ServerReaderException exception) {
            Assert.assertEquals("Read bad server xml check exception message ", "Invalid server xml ", exception.getMessage());
        }         
    }
}
